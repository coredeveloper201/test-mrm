<?php

namespace App\Http\Controllers;

use Storage;
use Session;
use App\up_media;
use Illuminate\Http\Request;

class UpMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    protected function getFileName($file)
    {
      return date('mdhms'). '.' . $file->extension();
    }

    public function store(Request $request)
    {
        $this-> validate(request(),[
           'image' => 'required|mimes:jpg,png,jpeg',
     
        ]);
         $up_media= new up_media();
         $filename = $this->getFileName($request->Image);
         $f=$request->Image;
        
         $path=Storage::putFileAs('public/image',$f,$filename);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\up_media  $up_media
     * @return \Illuminate\Http\Response
     */
    public function show(up_media $up_media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\up_media  $up_media
     * @return \Illuminate\Http\Response
     */
    public function edit(up_media $up_media)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\up_media  $up_media
     * @return \Illuminate\Http\Response
     */
    

    public function update(Request $request, up_media $up_media)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\up_media  $up_media
     * @return \Illuminate\Http\Response
     */
    public function destroy(up_media $up_media)
    {
        //
    }
}
